function biggest(n){
    let big=0
    while(n>0){
        let rem=n%10
        if(big<rem){
            big=rem
        }
        n=Math.floor(n/10)
    }
    return big
}
console.log(biggest(1234))
console.log(biggest(3426))