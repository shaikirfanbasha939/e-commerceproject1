let express = require('express')
let app= express()

var cors = require('cors')
app.use(cors())

let bodyparser=require('body-parser')
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false}))
 
app.use('/fetch',require('./components/fetch'))
app.use('/register',require('./components/register'))
app.use('/login',require('./components/login'))
app.use('/Delete',require('./components/delete'))
app.use('/update',require('./components/update'))
app.use('/product', require('./components/product'))
app.listen(3001,(req,res)=>{
    console.log('port is running 3001  ')
})