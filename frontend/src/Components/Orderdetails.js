import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input, Row, Col } from 'reactstrap';
import axios from 'axios'
function Orderdetails(args) {
  const [modal, setModal] = useState(false);
  const [formdata, setFormdata] = useState({
    email: '',
    password: '',
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
    radio2: ''
  })

  const handleInput = (e) => {
    const { name, value } = e.target
    setFormdata({
      ...formdata,

      [name]: value
    })
  }

  console.log(formdata)


  const toggle = (e) => {
    e.preventDefault();
    setModal(!modal);
  }


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.post('http://localhost:3001/register', formdata)
      console.log(response)
    //   alert("Ordered Successfully")


    } catch (err) {
      throw err
    }

  }
const handle=()=>{
    alert("Ordered Successfully")
    setModal(!modal)
}


  return (
    <div>
      <Button color="primary" onClick={toggle} className='btn-Warning'>
        Proceed to Buy
      </Button>
      

      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Deliver to this Address</ModalHeader>
        <ModalBody>
          <Form onSubmit={handleSubmit}>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <Label for="exampleEmail"
                    hidden>
                    Email
                  </Label>
                  <Input
                    id="exampleEmail"
                    name="email"
                    placeholder="Email"
                    type="email"
                    pattern="(?=.*\@)" title="Must contain '@' character"
                    value={formdata.email}
                    onChange={handleInput}
                    required
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="examplePassword">
                    Password
                  </Label>
                  <Input
                    id="examplePassword"
                    name="password"
                    placeholder="password "
                    type="password"
                    pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                    value={formdata.password}
                    onChange={handleInput}
                    required
                    
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Label for="exampleAddress">
               Delivery to Address1
              </Label>
              <Input
                id="exampleAddress"
                name="address1"
                placeholder="1234 Main St"
                value={formdata.address1}
                onChange={handleInput}
                required
              />
            </FormGroup>
            <FormGroup>
              <Label for="exampleAddress2">
                Address 2
              </Label>
              <Input
                id="exampleAddress2"
                name="address2"
                placeholder="Apartment, studio, or floor"
                value={formdata.address2}
                onChange={handleInput}
              />
            </FormGroup>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <Label for="exampleCity">
                    City
                  </Label>
                  <Input
                    id="exampleCity"
                    name="city"
                    value={formdata.city}
                    onChange={handleInput}
                  />
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="exampleState">
                    State
                  </Label>
                  <Input
                    id="exampleState"
                    name="state"
                    value={formdata.state}
                    onChange={handleInput}
                  />
                </FormGroup>
              </Col>
              <Col md={2}>
                <FormGroup>
                  <Label for="exampleZip">
                    Zip
                  </Label>
                  <Input
                    id="exampleZip"
                    name="zip"
                    value={formdata.zip}
                    onChange={handleInput}
                  />
                </FormGroup>
              </Col>
            </Row>
            <legend className="col-form-label col-sm-2">
              Gender
            </legend>
            <Col sm={10}>
              <FormGroup check>
                <Input
                  name="radio2"
                  type="radio"
                  value="Male"
                  // checked={formdata==='Male'}
                  onChange={handleInput}

                />
                {' '}
                <Label check>
                  Male
                </Label>
              </FormGroup>
              <FormGroup check>
                <Input
                  name="radio2"
                  type="radio"
                  value="Female"
                  // checked={formdata==='Female'}
                  onChange={handleInput}
                />
                {' '}
                <Label check>
                  Female
                </Label>
              </FormGroup>
            </Col>
            <Button type='submit' onClick={handle}>
                 Order Now
            </Button>
          </Form>

        </ModalBody>

      </Modal>
    </div>
  );
}


export default Orderdetails;