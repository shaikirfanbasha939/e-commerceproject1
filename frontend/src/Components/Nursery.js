import React from 'react'
import { Button } from 'react-bootstrap'

function Nursery() {
    return (
        <div>
            <div className='container'>
                <div className='row mt-4'>
                    <div className='col-sm-12 col-md-6 col-lg-6'><p className='fs-1 fw-bold'>From Our Nursery to Your Place: Plants to Enrich Your Space</p><p className='fs-5'>Our nursery provides a diverse range of quality plants to turn your garden into an oasis.Explore the variety and quality at our nursery, and let's grow together.</p><Button className='btn-success'>Book a Tour Now</Button></div>
                    <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/Untitled_design_2.jpg?width=1120&crop=center" className='w-100' /></div>
                </div>

            </div>

            <div className='container bg-warning mt-3 '>
                <h1 className='text-center '>Transform your garden with our wide selection of plants, expert advice, and superior customer service.</h1>
                <p className='text-center mt-3 fs-5'>At Heartyculture Nursery, our plant collection encompasses an exciting range of greenery, each nurtured to perfection. Whether you're seeking flowering plants to add a splash of color to your garden, indoor plants to purify your air, or hardy succulents to create a low-maintenance green space, we've got you covered. Our passion for plants is reflected in the diversity and health of our stock. Our plant categories include but are not limited to:</p>
                <div className='container'>
                    <div className='row mt-3'>
                        <div className='col-sm-12 col-md-6 col-lg-6'><img src='https://cdn.shopify.com/s/files/1/0487/7598/7354/files/2_59458964-93df-41da-8a89-515add00e716.jpg?width=1120' alt="" className='w-100' /><p className='text-center fs-1'>Indoor Plants</p><p className='text-center'>Bring nature inside with our selection of indoor plants, perfect for brightening up your interior spaces and improving air quality with our selection of annuals and perennials, perfect for every season.</p></div>
                        <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/3.jpg?width=1120" alt="" className='w-100' /><p className='text-center fs-1'>Trees & Shrubs</p><p className='text-center'>From fruit-bearing trees to evergreen shrubs, our nursery offers plants that provide structure and charm to your garden.</p></div>
                        <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/4.jpg?width=1120" alt="" className='w-100' /><p className='text-center fs-1'>Succulents & Cacti</p><p className='text-center'>Low-maintenance yet stunning, our range of succulents and cacti are ideal for those new to gardening or those with minimal time.</p></div>
                        <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/5.jpg?width=1120" alt="" className='w-100' /><p className='text-center fs-1'>Herbs & Vegetables</p><p className='text-center'>Cultivate your own kitchen garden with our range of edible plants, including a variety of herbs and vegetable seedlings.</p> </div>
                    </div>
                </div>
            </div>


            <div className='container mt-3'>
                <p className='fs-1 text-center'>Nursery & Beyond</p>
                <p className=' text-center'>Heartyculture Nursery started with a mission to protect and sustain endangered and endemic species of plants and trees of India. With 120-acre state-of-the-art Nursery in Hyderabad and 17 strategically located nurseries across India, we are home to more than 1500 different species belonging to 130 families. Mission to plant 30 million native and endemic trees across India by 2025. Heartyculture Nursery has a special focus on Indian Native species, aiming to promote and preserve the rich biodiversity of our country. We take pride in offering more than 256 rare, endangered, and endemic species of the Deccan Plateau, as well as 137 native species, for your selection. With an extensive inventory, we currently hold over 5 million plants at various stages, from seedlings to fully grown trees up to 20 feet in height.</p>
                <p className=' text-center mt-3'>All our plants and sapling are produced in house and grown with organic fertilizers, guaranteeing quality and healthy growth. We couple ancient wisdom with state of the art advanced infrastructure -  Tissue Culture lab, Poly Houses, Climate controlled Germination Chambers, Shaded Sheds and automated Irrigation systems. Heartyculture Nursery started with a mission to protect and sustain endangered and endemic species of plants and trees of India. Heartyculture Nursery offers a range of services to support all your plant needs and enhances your plant journey. Our experienced team of horticulturists and plant enthusiasts is dedicated to providing exceptional service and expert advice.</p>
                <div className='row mt-4'>
                    <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/20.jpg?width=1120&crop=center" className='w-100' /><p className='text-center fs-3'>Plant selection assistance</p><p className='text-center'>Our team of horticultural experts is always available to help you choose the right plants for your home or garden and provide tips on their care.</p></div>
                    <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/21.jpg?width=1120&crop=center" className='w-100' /><p className='text-center fs-3'>Landscape Design and Consultation</p><p className='text-center'>We offer delivery services and can assist with planting to ensure your new green friends get off to the best start in their new home.</p></div>
                    <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/22.jpg?width=1120&crop=center" className='w-100' /><p className='text-center fs-3'>Plant Installation and Maintenance</p><p className='text-center'>Our experienced landscape designers can help you plan and create the garden of your dreams, from conceptualization to plant selection and placement.</p></div>
                    <div className='col-sm-12 col-md-6 col-lg-6'><img src="https://cdn.shopify.com/s/files/1/0487/7598/7354/files/23.jpg?width=1120&crop=center" className='w-100' /><p className='text-center fs-3'>Gifting Curation</p><p className='text-center'>Join our regular gardening workshops and learn about plant care, potting, propagation, and more from our knowledgeable staff.</p></div>
                </div>
            </div>
        </div>
    )
}

export default Nursery