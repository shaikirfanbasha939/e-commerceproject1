import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import axios from 'axios'
import './Products.css'
import { useCart } from 'react-use-cart'



function Products() { 
  const [product, setProduct] = useState([])
  
  
  
  useEffect(()=>{
    const fetchData=async()=>{
      try{
        let response =await axios.get("http://localhost:3001/fetch")
        console.log(response.data)
        setProduct(response.data)
      }catch(err){
        throw err
      }
    }
    fetchData()
  },[])
  
  const { addItem } = useCart();

    // const addToCart = (item) =>{
  
    //     console.log(item)
    //     addItem(item)
    // }
  // const item= async (e)=>{

  //     <Cart add={e}/>

  //   // try{
  //   //   let response = await axios.post('http://localhost:3001/register',e)
  //   //   console.log(response.data)
  //   // }catch(err){
  //   //   throw err
  //   // }
  //   // let response=  axios.post('http://localhost:3001/register',product)
  //   // console.log(response)
  
  // }

  return (
    <div>
      <div className='mt-3'>
        <h4 className='text-info text-center'>Our Top Selling Plants & Trees</h4>
        <div className='container '>
          <div className='row mt-3 '>
            {product.filter((e)=>(e.Img ,e.Name ,e.price)).map((e) => (
              // <Cards key={e.Img} data={e}/>
              <div className='col-xs-6 col-md-4 col-lg-3 justify-content- around border-0 mt-3 '  id="image" key={e.Img}>
                <img src={e.Img} alt="images" className='w-100 rounded-3'  />
                <div className='card-body'>
                  <p className='text-seconadry text-center' >{e.Name}</p>
                  <p className='text-danger text-center'  >Rs. {e.price}</p>
                </div>
                <div className='footer text-center '> <Button className='btn btn-primary px-4' onClick={()=>addItem(e)}>Add to Cart</Button></div>
              </div>
            ))}
          </div>
        </div>
      </div>

  
    </div>
  )
}

export default Products