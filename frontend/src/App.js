import logo from './logo.svg';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './Components/Home';
import { Navbar, Container, Nav, CardText, Card } from 'react-bootstrap';
import Login from './Components/Login';
import Contact from './Components/Contact';
import Products from '../src/Components/Products/Products';
import Cart from './Components/Cart';
import Nursery from './Components/Nursery';
import { BiCart } from 'react-icons/bi';
import { useCart } from 'react-use-cart';

function App() {
  const {
    isEmpty,
    totalItems,
  } = useCart();
  return (
    <>
      {/* <Header/> */}
      <div className='container'>
        <Navbar expand='lg' className="bg-body-secondary fixed-top mt-">

          <Navbar.Brand href="/"><img src="https://www.heartyculturenursery.com/cdn/shop/files/logo_horizontal_1912x500.png?v=1614351315" style={{ width: '150px', height: '40px' }} /></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <div className='container'>
              <Nav className="d-flex justify-content-evenly  ms-auto">
                <Nav.Link href="/"><h6>Home</h6></Nav.Link>
                <Nav.Link href="/products"><h6>Online Shop</h6></Nav.Link>
                <Nav.Link href="/nursery"><h6>Our Nursery & Services</h6></Nav.Link>
                {/* <Nav.Link href="/service"><h6>Landscaping Services</h6></Nav.Link> */}
                <Nav.Link href="/contact"><h6>Contact Us</h6></Nav.Link>
                <Nav.Link href="/" ><Login /></Nav.Link>
                {/* <Nav.Link href="/cart"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" className="bi bi-cart3" viewBox="0 0 16 16">
                  <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5M3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4m7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4m-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2m7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2" />
                </svg></Nav.Link> */}
                <Nav.Link href="/cart" className='d-flex'>
                  <BiCart size="2rem" />
                  {!isEmpty && <span className='mt--5 text-danger  '>{totalItems}</span>}
                </Nav.Link>

              </Nav>
            </div>
          </Navbar.Collapse>

        </Navbar>

      </div> <br />
      <div className='mt-5'>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/nursery" element={<Nursery />} />
            <Route path="/products" element={<Products />} />
            <Route path="/contact" element={<Contact />} />
            {/* <Route path="/login" element={<Login />} /> */}
            <Route path="/cart" element={<Cart />} />
          </Routes>
        </BrowserRouter>
      </div>
    </>
  );
}

export default App;
